import { PustakPage } from './app.po';

describe('pustak App', () => {
  let page: PustakPage;

  beforeEach(() => {
    page = new PustakPage();
  });

  it('should display welcome message', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('Welcome to app!!');
  });
});
